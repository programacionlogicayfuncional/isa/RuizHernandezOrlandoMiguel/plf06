(ns plf06.core
  (:gen-class))

(def alfabeto_simbolos
  (seq "0123456789!\"#$%+'(),&-./;:<=>?@[/]^`{|}~aábcdeéfghiíjklmnñoópqrstuúüvwxyzAÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ0123456789!\"#$%+'(),&-./;:<=>?@[/]^`{|}~"))

(defn cifrado-rot-13
  [x]
  (let [f (zipmap alfabeto_simbolos (drop 13 (take 147 (conj alfabeto_simbolos))))]
    (apply str (replace f x)))) 
 
(defn -main 
  [& args]
  (if (empty? args)
    (println "Error Cadena vacía")
    (println (cifrado-rot-13 (apply str args))))) 


